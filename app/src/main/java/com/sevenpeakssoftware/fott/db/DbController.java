package com.sevenpeakssoftware.fott.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sevenpeakssoftware.fott.entity.Feed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alla on 10/14/15.
 */
public class DbController {

    private DbHelper dbHelper;

    public DbController(Context context) {
        dbHelper = new DbHelper(context);
    }

    public void addFeeds(List<Feed> feeds) {
        // clear all previous records in database, because we need to override them
        deleteAll();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        for (Feed feed : feeds) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DbHelper.ID_COLUMN, feed.getId());
            contentValues.put(DbHelper.TITLE_COLUMN, feed.getTitle());
            contentValues.put(DbHelper.IMAGE_COLUMN, feed.getImage());
            contentValues.put(DbHelper.INGRESS_COLUMN, feed.getIngress());
            contentValues.put(DbHelper.DATE_COLUMN, feed.getDateTime());
            contentValues.put(DbHelper.SUBJECT_COLUMN, feed.getSubject());
            contentValues.put(DbHelper.DESCRIPTION_COLUMN, feed.getDescription());

            db.insert(DbHelper.TABLE_FEEDS, null, contentValues);
        }

        db.close();
    }

    public List<Feed> getFeeds() {
        List<Feed> result = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DbHelper.TABLE_FEEDS, null, null, null, null, null, null);

        if(cursor.moveToFirst()) {
            //Get index of columns
            int idColIndex = cursor.getColumnIndex(DbHelper.ID_COLUMN);
            int titleColIndex = cursor.getColumnIndex(DbHelper.TITLE_COLUMN);
            int imageColIndex = cursor.getColumnIndex(DbHelper.IMAGE_COLUMN);
            int ingressColIndex = cursor.getColumnIndex(DbHelper.INGRESS_COLUMN);
            int dateColIndex = cursor.getColumnIndex(DbHelper.DATE_COLUMN);
            int subjectColIndex = cursor.getColumnIndex(DbHelper.SUBJECT_COLUMN);
            int descriptionColIndex = cursor.getColumnIndex(DbHelper.DESCRIPTION_COLUMN);

            do {
                //Read a record from DB
                int id = cursor.getInt(idColIndex);
                String title = cursor.getString(titleColIndex);
                String image = cursor.getString(imageColIndex);
                String ingress = cursor.getString(ingressColIndex);
                String date = cursor.getString(dateColIndex);
                String subject = cursor.getString(subjectColIndex);
                String description = cursor.getString(descriptionColIndex);

                result.add(new Feed(id, title, date, ingress, image, description, subject));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return result;
    }

    private void deleteAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.delete(DbHelper.TABLE_FEEDS, null, null);
    }
}
