package com.sevenpeakssoftware.fott.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by alla on 10/14/15.
 */
public class DbHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "fott_database";
    public static final int DB_VERSION = 1;

    public static final String TABLE_FEEDS = "feeds";

    public static final String ID_COLUMN = "id";
    public static final String TITLE_COLUMN = "title";
    public static final String IMAGE_COLUMN = "image";
    public static final String INGRESS_COLUMN = "ingress";
    public static final String DATE_COLUMN = "date";
    public static final String SUBJECT_COLUMN = "subject";
    public static final String DESCRIPTION_COLUMN = "description";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_FEEDS + "("
                + ID_COLUMN + " INTEGER PRIMARY KEY, "
                + TITLE_COLUMN + " TEXT, "
                + IMAGE_COLUMN + " TEXT, "
                + INGRESS_COLUMN + " TEXT, "
                + DATE_COLUMN + " TEXT, "
                + SUBJECT_COLUMN + " TEXT, "
                + DESCRIPTION_COLUMN + " TEXT" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
