package com.sevenpeakssoftware.fott.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.sevenpeakssoftware.fott.R;
import com.sevenpeakssoftware.fott.Util.DateUtil;
import com.sevenpeakssoftware.fott.entity.Feed;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    public static final String FEED_PARAM = "feed_param";

    private Feed feed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        feed = getIntent().getParcelableExtra(FEED_PARAM);
        initViews();
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TextView tvToolbarTitle = (TextView) findViewById(R.id.tv_app_bar_title);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        TextView tvDate = (TextView) findViewById(R.id.tv_date);
        TextView tvIngress = (TextView) findViewById(R.id.tv_ingress);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        TextView tvSubject = (TextView) findViewById(R.id.tv_subject);
        TextView tvDescription = (TextView) findViewById(R.id.tv_description);

        tvToolbarTitle.setText(feed.getTitle());
        tvTitle.setText(feed.getTitle());
        tvDate.setText(DateUtil.formatDate(feed.getDateTime()));
        tvIngress.setText(feed.getIngress());
        tvSubject.setText(feed.getSubject());
        tvDescription.setText(feed.getDescription());

        Picasso.with(DetailsActivity.this)
                .load(feed.getImage())
                .placeholder(R.drawable.background)
                .into(imageView);
    }
}
