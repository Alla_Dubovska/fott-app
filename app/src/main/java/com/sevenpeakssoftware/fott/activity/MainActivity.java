package com.sevenpeakssoftware.fott.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.sevenpeakssoftware.fott.FottApp;
import com.sevenpeakssoftware.fott.R;
import com.sevenpeakssoftware.fott.adapter.FeedAdapter;
import com.sevenpeakssoftware.fott.entity.Feed;
import com.sevenpeakssoftware.fott.entity.FeedResponse;
import com.sevenpeakssoftware.fott.network.FeedService;

import java.util.Arrays;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener{
    private static final String TAG = "MainActivity";

    private List<Feed> feeds;

    private ProgressBar progressBar;
    private ListView lvFeeds;

    private FeedAdapter feedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        loadFeeds();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        lvFeeds = (ListView) findViewById(R.id.lv_feeds);
        lvFeeds.setOnItemClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    private void loadFeeds() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.apphusetreach.no")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FeedService service = retrofit.create(FeedService.class);

        Call<FeedResponse> searchCall = service.getAllFeeds();

        searchCall.enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Response<FeedResponse> response, Retrofit retrofit) {
                feeds = Arrays.asList(response.body().getContent());
                feedAdapter = new FeedAdapter(MainActivity.this, feeds);
                lvFeeds.setAdapter(feedAdapter);

                // Update feeds in the database
                FottApp.get().getDbController().addFeeds(feeds);

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {
                // if can't get data from server just load it from database
                feeds = FottApp.get().getDbController().getFeeds();
                feedAdapter = new FeedAdapter(MainActivity.this, feeds);
                lvFeeds.setAdapter(feedAdapter);

                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.FEED_PARAM, feeds.get(i));
        startActivity(intent);
    }
}
