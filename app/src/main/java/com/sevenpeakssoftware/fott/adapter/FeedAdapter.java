package com.sevenpeakssoftware.fott.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sevenpeakssoftware.fott.R;
import com.sevenpeakssoftware.fott.Util.DateUtil;
import com.sevenpeakssoftware.fott.entity.Feed;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Simple adapter for feeds.
 *
 * Created by alla on 10/13/15.
 */
public class FeedAdapter extends BaseAdapter {

    private Context context;
    private List<Feed> feeds;

    public FeedAdapter(Context context, List<Feed> feeds) {
        this.context = context;
        this.feeds = feeds;
    }

    @Override
    public int getCount() {
        return feeds.size();
    }

    @Override
    public Object getItem(int i) {
        return feeds.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.lv_item_feed, null);
            viewHolder = new ViewHolder();
            viewHolder.tvTitle = (TextView) view.findViewById(R.id.tv_title);
            viewHolder.tvDate = (TextView) view.findViewById(R.id.tv_date);
            viewHolder.tvIngress = (TextView) view.findViewById(R.id.tv_ingress);
            viewHolder.imageView = (ImageView) view.findViewById(R.id.imageView);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Feed feed = (Feed) getItem(i);
        viewHolder.tvTitle.setText(feed.getTitle());
        viewHolder.tvDate.setText(DateUtil.formatDate(feed.getDateTime()));
        viewHolder.tvIngress.setText(feed.getIngress());

        Picasso.with(context)
                .load(feed.getImage())
                .placeholder(R.drawable.background)
                .into(viewHolder.imageView);
        return view;
    }

    public static class ViewHolder {
        public TextView tvTitle;
        public TextView tvDate;
        public TextView tvIngress;
        public ImageView imageView;
    }
}
