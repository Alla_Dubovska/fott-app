package com.sevenpeakssoftware.fott.entity;

/**
 * Created by alla on 10/13/15.
 */
public class FeedResponse {

    private String status;
    private Feed[] content;

    public String getStatus() {
        return status;
    }

    public Feed[] getContent() {
        return content;
    }
}
