package com.sevenpeakssoftware.fott.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Entity class for feed.
 * <p>
 * Created by alla on 10/13/15.
 */
public class Feed implements Parcelable {

    private int id;
    private String title;
    private String dateTime;
    private String ingress;
    private String image;
    private FeedContent[] content;

    public Feed(int id, String title, String dateTime, String ingress, String image, String description, String subject) {
        this.id = id;
        this.title = title;
        this.dateTime = dateTime;
        this.ingress = ingress;
        this.image = image;

        content = new FeedContent[1];
        content[0] = new FeedContent();
        content[0].description = description;
        content[0].subject = subject;
    }

    public Feed(Parcel parcel) {
        id = parcel.readInt();
        title = parcel.readString();
        ingress = parcel.readString();
        image = parcel.readString();

        content = new FeedContent[1];
        content[0] = new FeedContent();
        content[0].description = parcel.readString();
        content[0].subject = parcel.readString();

        dateTime = parcel.readString();
    }

    public String getTitle() {
        return title;
    }

    public String getIngress() {
        return ingress;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getImage() {
        return image;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return content[0].description;
    }

    public String getSubject() {
        return content[0].subject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(ingress);
        parcel.writeString(image);
        parcel.writeString(getDescription());
        parcel.writeString(getSubject());
        parcel.writeString(dateTime);
    }

    public static final Parcelable.Creator<Feed> CREATOR = new Parcelable.Creator<Feed>() {

        @Override
        public Feed createFromParcel(Parcel parcel) {
            return new Feed(parcel);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };

    public static class FeedContent {
        private String subject;
        private String description;
    }
}
