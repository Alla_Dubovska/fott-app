package com.sevenpeakssoftware.fott;

import android.app.Application;

import com.sevenpeakssoftware.fott.db.DbController;

/**
 * Created by alla on 10/14/15.
 */
public class FottApp extends Application {

    private static FottApp app;

    private DbController dbController;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        dbController = new DbController(this);
    }

    public static FottApp get() {
        return app;
    }

    public DbController getDbController() {
        return dbController;
    }
}
