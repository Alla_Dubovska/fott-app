package com.sevenpeakssoftware.fott.network;

import com.sevenpeakssoftware.fott.entity.FeedResponse;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Retrofit interface to get feed.
 * Created on 10/13/15.
 *
 * @author Alla Dubovska
 */
public interface FeedService {
    @GET("/application/40495/article/get_articles_list")
    Call<FeedResponse> getAllFeeds();
}