package com.sevenpeakssoftware.fott.Util;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Util class for formatting date.
 *
 * Created by alla on 10/14/15.
 */
public class DateUtil {

    public DateUtil() {
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatDate(String dateTime) {
        try {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            Date date = format.parse(dateTime);
            Date currentDate = new Date();
            SimpleDateFormat sdf;
            if (currentDate.getDate() == date.getDate() &&
                    currentDate.getMonth() == date.getMonth() &&
                    currentDate.getYear() == date.getYear()) {
                sdf = new SimpleDateFormat("Today, HH:mm");
            } else {
                sdf = new SimpleDateFormat("dd MMMM, HH:mm");
            }
            return sdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
